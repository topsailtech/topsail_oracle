#!/bin/env ruby

require 'erb'

require File.dirname(__FILE__) + '/../lib/topsail/oracle/database_arguments'
require File.dirname(__FILE__) + '/../lib/topsail/oracle/oracle_object_type'

include Topsail::Oracle

INSERT_TEMPLATE = %q{
insert into <%= table %> (<%= columns.join(', ')%>)
  values (<%= values.join(', ')%>);
}

SEQUENCE_TEMPLATE = %q{
alter sequence <%=table%>_SEQ increment by <%=last_id%>;
select <%=table%>_SEQ.nextval from dual;
alter sequence <%=table%>_SEQ increment by 1;
}

def print_inserts(conn, schema, table, out=$stdout)

    insert_template = ERB.new(INSERT_TEMPLATE)
    sequence_template = ERB.new(SEQUENCE_TEMPLATE)
    
    # get the column names and data types

    columns = []
    data_types = []
    
    cur = conn.exec("select column_name, data_type from all_tab_columns where table_name = '#{table}' and owner = '#{schema.upcase}'")

    while (row = cur.fetch)
        columns << row[0]
        data_types << row[1]
    end    
    
    cur.close

    # don't bother if this isn't a valid tale
    return if columns.empty?

    # get all the rows in the table
    
    query = "select #{columns.join(',')} from #{table}"
    query += ' order by id' if columns.include?("ID") or columns.include?("id")

    cur = conn.exec(query)
    count = 0
    
    while (row = cur.fetch)

      count += 1
      values = []

      data_types.each_index do | i |
        
        value = row[i]

        if value.nil?
          value = 'null'
        elsif data_types[i] == 'DATE'
          value = value.to_s
          value = "to_date('#{value}', 'yyyy/mm/dd hh24:mi:ss')" 
        elsif data_types[i] != 'NUMBER'
          
          if data_types[i] == 'CLOB'
            value = value.read
          else
            value = value.to_s
          end
          
          value = value.gsub(/'/, "''").gsub(/\&/, '\\\&')
          value = "'#{value}'"
        
        end
        
        values << value
      end
      
      out.puts insert_template.result(binding)
    
    end    
    
    cur.close
    
    if count > 0 and columns.include?("ID") or columns.include?("id")
      # get the last id
      last_id = nil
      conn.exec("select max(id) from #{table}") { | row | last_id = row[0].to_i }
      out.puts sequence_template.result(binding)
    end
    
end


# main program

args = DatabaseArguments.new

conn = OCI8.new(args.user, args.password, args.database)

if !args.script.nil?
  out = File.open(args.script, 'w')
else
  out = $stdout 
end

out.puts "set escape on;\n"

if ARGV.size > 1
  ARGV[1...ARGV.size].each do |table|
    print_inserts(conn, args.schema, table.upcase, out)
  end
else
  OracleObjectType::TABLE.each(conn, args.schema) do | table |
    print_inserts(conn, args.schema, table, out)
  end
end

conn.logoff
out.close if !args.script.nil?
