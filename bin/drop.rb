#!/bin/env ruby

require File.dirname(__FILE__) + '/../lib/topsail/oracle/database_arguments'
require File.dirname(__FILE__) + '/../lib/topsail/oracle/oracle_object_type'

###############################################################################
# drop.rb
#
# A commandline tool for droping all the objects in an Oracle schema.
# By default this script drops everything whenever it is run.  Use the
# "pretend" option (-p) to avoid this.  The "output" option (-o) will
# generate a SQL script with all the drop statements in it.
# 
###############################################################################

include Topsail::Oracle

#
# The commandline arguments for the script.
# 
class DropArguments < DatabaseArguments
    
    attr_accessor :pretend, :quiet

    def initialize()
      @pretend = false
      @verbose = false
      super()
    end
        
    def create_parser
      
      parser = super
      
      parser.on("-p", "--pretend", "Do not execute the drop statements") do |opt|
        @pretend = true
      end

      parser.on("-q", "--quiet", "Quiet mode") do |opt|
        @quiet = true
      end
           
    end
    
end


#
# MAIN PROGRAM
#

# parse commandline arguments

args = DropArguments.new

# generate the drop statements

statements = []

conn = OCI8.new(args.user, args.password, args.database)

conn.exec('purge recyclebin')

# tables
OracleObjectType::TABLE.each(conn, args.schema) do | table |
    statements << "drop table \"#{table}\" cascade constraints"
end

# others
for type in [ OracleObjectType::VIEW, OracleObjectType::SEQUENCE,
    OracleObjectType::FUNCTION, OracleObjectType::PROCEDURE,
    OracleObjectType::PACKAGE, OracleObjectType::TYPE ]

  type.each(conn, args.schema) do | name |
      statements << "drop #{type.name} #{name}"
  end

end

statements << 'purge recyclebin'

# process the statements

for stmt in statements
    puts stmt unless args.quiet
    conn.exec(stmt) unless args.pretend
end

if args.script
    File.open(args.script, 'w') do | file |
        statements.each { | stmt | file.puts stmt + ';' }
        file.puts 'exit;'
    end
end

conn.logoff

