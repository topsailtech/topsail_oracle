#!/bin/env ruby

require File.dirname(__FILE__) + '/../lib/topsail/oracle/database_arguments'
require File.dirname(__FILE__) + '/../lib/topsail/oracle/oracle_object_type'

include Topsail::Oracle

args = DatabaseArguments.new

conn = OCI8.new(args.user, args.password, args.database)

conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'STORAGE', false); end;")
conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'TABLESPACE', false); end;")
conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'SEGMENT_ATTRIBUTES', false); end;")
conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'CONSTRAINTS', false); end;")
conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'REF_CONSTRAINTS', false); end;")
conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'CONSTRAINTS_AS_ALTER', false); end;")
conn.exec("begin dbms_metadata.set_transform_param(dbms_metadata.session_transform, 'SQLTERMINATOR', true); end;")
conn.exec('purge recyclebin')

TYPES = [ OracleObjectType::TABLE, OracleObjectType::CONSTRAINT, OracleObjectType::REF_CONSTRAINT,
  OracleObjectType::NON_CONSTRAINT_INDEX, OracleObjectType::SEQUENCE, OracleObjectType::TYPE,
  OracleObjectType::TRIGGER, OracleObjectType::FUNCTION, OracleObjectType::PROCEDURE,
  OracleObjectType::PACKAGE, OracleObjectType::VIEW ]

if args.script.nil?
    
    # print to standard out
    
    TYPES.each do | type |
      type.print_all_ddl(conn, args.schema)
    end
    
else
    
    # write to the file
     
    File.open(args.script, 'w') do | file |
        
        TYPES.each do | type |
            type.print_all_ddl(conn, args.schema, file)
        end
    
    end

end

conn.logoff
