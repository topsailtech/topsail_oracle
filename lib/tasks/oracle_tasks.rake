
namespace :oracle do

  desc "Drop all the objects in an Oracle schema."
  task :drop => :environment do
    conf = ActiveRecord::Base.configurations[Rails.env]
    drop_script = File.dirname(__FILE__) + '/../../bin/drop.rb'
    ruby "-rubygems #{drop_script} -u #{conf['username']} -w #{conf['password']} #{conf['database']}"
  end

  desc "Create a new schema."
  task :create => :environment do
    create = "#{Rails.root}/db/create.sql"
    if File.exists?(create)
      conf = ActiveRecord::Base.configurations[Rails.env]
      sh "sqlplus -L #{conf['username']}/#{conf['password']}@#{conf['database']} @#{create}"
    end
  end

  desc "Drop and create the schema, without applying alter script or migrations."
  task :recreate => [ :drop, :create ]
  
  desc "Alter a new schema with the current alter script."
  task :alter => :environment do
    alter = "#{Rails.root}/db/alter.sql"
    if File.exists?(alter)
      conf = ActiveRecord::Base.configurations[Rails.env]
      sh "sqlplus -L #{conf['username']}/#{conf['password']}@#{conf['database']} @#{alter}"
    end
  end

  desc "Load data with the current insert script."
  task :insert => :environment do
    if File.exists?('db/insert.sql')
      conf = ActiveRecord::Base.configurations[Rails.env]
      sh "sqlplus -L #{conf['username']}/#{conf['password']}@#{conf['database']} @db/insert.sql"
    end
  end
  
  desc "Rebuild the schema"
  task :rebuild => [ :drop, :create, :alter, 'db:migrate', 'db:seed', :insert ]

  desc "Rebuild the schema and load the fixtures"
  task :rebuild_with_fixtures => [ :drop, :create, :alter, 'db:migrate', 'db:seed', :insert, 'db:fixtures:load' ]
  
  desc "Recreate the create.sql script by extracting the DDL from the database."
  task :extract_ddl => :environment do
    
    conf = ActiveRecord::Base.configurations[Rails.env]
    
    # create statements
    
    create = "#{Rails.root}/db/create.sql"
    extract_script = File.dirname(__FILE__) + '/../../bin/extract_ddl.rb'
    ruby "-rubygems #{extract_script} -u #{conf['username']} -w #{conf['password']} -o #{create} #{conf['database']}"
    
    # schema_info or schema_migrations value if it exists
    extract_script = File.dirname(__FILE__) + '/../../bin/extract_inserts.rb'
    
    if Rails::VERSION::STRING.to_f >= 2.1
      schema_table = 'schema_migrations'
    else
      schema_table = 'schema_info'
    end
    
    ruby "-rubygems #{extract_script} -u #{conf['username']} -w #{conf['password']} #{conf['database']} #{schema_table} >> #{create}"
    
    sh "echo 'exit;' >> #{create}"
    
  end
  
  desc "Remove everything in alter.sql."
  task :empty_alter_script do
    alter = "#{Rails.root}/db/alter.sql"
    if File.exists?(alter)
      sh "echo 'exit;' > #{alter}"
    end
  end

  desc "Recreate the insert.sql script by generating inserts for all the data currently in the database."
  task :extract_inserts => :environment do
    
    conf = ActiveRecord::Base.configurations[Rails.env]
    insert = "#{Rails.root}/db/insert.sql"
    extract_script = File.dirname(__FILE__) + '/../../bin/extract_inserts.rb'
    command = "-rubygems #{extract_script} -u #{conf['username']} -w #{conf['password']} -o #{insert} #{conf['database']}"
    
    unless ENV['FIXTURES'].nil?
      fixtures = ENV['FIXTURES'].split(',')
      fixtures.each do |fixture|
        command += " #{fixture}"
      end
    end
    
    ruby command
    sh "echo 'exit;' >> #{insert}"
    
  end
  
  desc "Reset create.sql and alter.sql by rebuilding the schema, extracting the create.sql DDL from it and cleaning out alter.sql"
  task :reset => [ :rebuild, :extract_ddl, :empty_alter_script ]
  
end
