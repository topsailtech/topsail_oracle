module Topsail
  module Oracle
    class Railtie < Rails::Railtie

      rake_tasks do
        #load File.expand_path("#{File.dirname(__FILE__)}/../../tasks/oracle_tasks.rake")
        load "tasks/oracle_tasks.rake"
      end

    end
  end
end
