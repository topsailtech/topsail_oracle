require 'oci8'
require 'stringio'

module Topsail

  module Oracle

    #
    # A type of object in Oracle (table, sequence, etc) and the information
    # needed to drop all objects of that type
    # 
    class OracleObjectType

      attr_reader :name, :query, :dependent_types
      
      def initialize(name, query, dependent_types=[])       
          @name = name
          @query = query
          @dependent_types = dependent_types
      end

      # Find the names of every object of this type owned by the specified owner
      # and pass each one to the provided code block
      def each(connection, owner)
        
        begin
          
          cursor = connection.parse(@query)
          cursor.bind_param(':owner', owner)
          cursor.exec
          
          while (row = cursor.fetch)
              name = row[0]
              yield name
          end 

        ensure
          cursor.close
        end

      end
      
      # Find the names of every object of this type owned by the specified owner
      # and return then as an array
      def get_names(connection, owner)
        
        names = []
        
        each(connection, owner) do | name |
          names << name
        end
        
        return names
        
      end

      # Prints the DDL for all objects of this type in the schema to the provided file
      def print_all_ddl(connection, schema, out=$stdout)
        
        each(connection, schema) do | object_name |
          print_ddl(connection, object_name, schema, out)
        end
        
      end
      
      # Print the DDL for the object in the schema to the provided file, which is
      # standard out by default
      def print_ddl(connection, object_name, schema, out=$stdout)
        
        ddl = get_ddl(connection, object_name, schema)
        out.puts(ddl)
        
        for type in @dependent_types
          ddl = get_dependent_ddl(connection, type, object_name, schema)
          out.puts(ddl)
        end
        
      end
      
      # Get the DDL for a specific object
      def get_ddl(connection, object_name, schema)
        
        query = "select dbms_metadata.get_ddl('#{@name}', '#{object_name}', '#{schema}') from dual"
        return get_ddl_from_query(connection, query, schema)
       
      end
      
      # Get the DDL for all objects of this type that are dependent of the specified table
      def get_dependent_ddl(connection, type, table, schema)

        ddl = ''
        
        begin
            query = "select dbms_metadata.get_dependent_ddl('#{type}', '#{table}', '#{schema}') from dual"
            ddl = get_ddl_from_query(connection, query, schema)
        rescue OCIError
            # ignore cases where there are no dependent objects of the specified type
            # TODO figure out a better way to handle these cases that doesn't involve swallowing errors
        end
        
        return ddl
      end
          
      def get_ddl_from_query(connection, query, schema)
    
        begin
          cursor = connection.exec(query);

          row = cursor.fetch
          ddl = row[0]

          strio = StringIO.new

          until(ddl.eof?)
              strio.puts ddl.read
          end

          # remove the schema from the object names
          str = strio.string
          str.gsub!(Regexp.new("\"#{schema}\"\."), "")
          str.strip
            
        ensure
            cursor.close
        end
      end
      
      # constants
      
      TABLE = OracleObjectType.new('TABLE', 'select table_name from all_tables where owner = :owner order by table_name', [ 'COMMENT' ] )
      VIEW = OracleObjectType.new('VIEW', 'select view_name from all_views where owner = :owner order by view_name')
      INDEX = OracleObjectType.new('INDEX', 'select index_name from all_indexes where owner = :owner order by index_name')
      SEQUENCE = OracleObjectType.new('SEQUENCE', 'select sequence_name from all_sequences where sequence_owner = :owner order by sequence_name')
      TRIGGER = OracleObjectType.new('TRIGGER', 'select trigger_name from all_triggers where owner = :owner order by trigger_name')
      TYPE = OracleObjectType.new('TYPE', "select distinct type_name, typecode from all_types where owner = :owner order by typecode, type_name")
      FUNCTION = OracleObjectType.new('FUNCTION', "select distinct name from all_source where type = 'FUNCTION' and owner = :owner order by name")
      PROCEDURE = OracleObjectType.new('PROCEDURE', "select distinct name from all_source where type = 'PROCEDURE' and owner = :owner order by name")
      PACKAGE = OracleObjectType.new('PACKAGE', "select distinct name from all_source where type = 'PACKAGE' and owner = :owner order by name")
      CONSTRAINT = OracleObjectType.new('CONSTRAINT', "select constraint_name from all_constraints where owner = :owner and upper(constraint_type) != 'R' order by constraint_name")
      REF_CONSTRAINT = OracleObjectType.new('REF_CONSTRAINT', "select constraint_name from all_constraints where owner = :owner and upper(constraint_type) = 'R' order by constraint_name")
      
      NON_CONSTRAINT_INDEX = OracleObjectType.new('INDEX',
        'select index_name from all_indexes where owner = :owner and not exists (
          select 1 from all_constraints where owner = :owner and all_constraints.constraint_name = all_indexes.index_name)
          order by index_name')

      ALL = [ TABLE, CONSTRAINT, REF_CONSTRAINT, INDEX, SEQUENCE, TYPE, TRIGGER, FUNCTION, PROCEDURE, PACKAGE, VIEW ]

      private :get_ddl_from_query
     
    end

  end

end

