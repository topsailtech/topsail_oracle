require 'optparse'

module Topsail

  module Oracle

    #
    # The commandline arguments for a database tool.  This class handles parsing
    # and validating the arguments.
    # 
    class DatabaseArguments
        
      attr_accessor :schema, :user, :password, :database, :script
      
      def initialize

        parser = create_parser
        parser.parse!
        
        abort parser.help if (ARGV.size < 1) 

        @database = ARGV[0]
          
        if @user.nil?
          print 'User: '
          @user = $stdin.gets.chomp
        end
          
        if @password.nil?
          print 'Password: '
          @password = $stdin.gets.chomp
        end
          
        @schema = @user if @schema.nil?
        @schema.upcase!

      end

      def create_parser
        
        OptionParser.new do |parser|
          parser.banner = "Usage: [options] <database>"
          parser.separator ""
          parser.separator "Options:"

          parser.on("-u", "--user USER", "The database user name") do |opt|
            @user = opt
          end
         
          parser.on("-w", "--password PASSWORD", "The database password") do |opt|
            @password = opt
          end
            
          parser.on("-s", "--schema SCHEMA", "The database schema") do |opt|
            @schema = opt
          end

          parser.on("-o", "--output SCRIPT", "Generate a output script") do |opt|
            @script = opt
          end
                
        end
        
      end

    end

  end
    
end
