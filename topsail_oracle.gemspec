# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "topsail/oracle/version"

Gem::Specification.new do |s|
  s.name        = "topsail_oracle"
  s.version     = Topsail::Oracle::VERSION
  s.authors     = ["Mark Roghelia"]
  s.email       = ["mroghelia@topsailtech.com"]
  s.summary     = %q{Oracle support utilities}
  s.files         = `git ls-files`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
